import { expect } from "chai";


export function checkStatusCode(response, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(statusCode);
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(response.timings.phases.total, `Response time should be less than ${maxResponseTime}ms`).to.be.lessThan(
        maxResponseTime
    );
}
export function checkJsonSchema(response, jsonSchema) {
    expect(response.body, `Json schema is correct`).to.be.jsonSchema(jsonSchema);
}
export function checkValidationErrorMessage(response) {
    expect(response.body.message).to.be.equal('Validation errors')
}
export function checkResponseLenghtsMoreThanOne(response) {
    expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
} 
export function checkUserData(response, avatar, userName, email, userId) {
    expect(response.body.avatar).to.be.equal(avatar);
    expect(response.body.userName).to.be.equal(userName);
    expect(response.body.email).to.be.equal(email);
    expect(response.body.id).to.be.equal(userId);
} 
export function checkUserDataAfterRegistration(response, avatar, userName, email, userId) {
    expect(response.body.user.avatar).to.be.equal(avatar);
    expect(response.body.user.userName).to.be.equal(userName);
    expect(response.body.user.email).to.be.equal(email);
    expect(response.body.user.id).to.be.equal(userId);
} 
export function checkErrorCode(response, errorMessage: 'Entity User was not found.'|'Invalid username or password.',code: 2 | 3) {
    expect(response.body.error, `Error message - ${errorMessage}`).to.be.equal(errorMessage);
    expect(response.body.code, `Error code  - ${code}`).to.be.equal(code);
} 
    