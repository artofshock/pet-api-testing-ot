import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import { RegistrationController } from "../lib/controllers/registration.controller";
import { v4 as guid } from "uuid";
import { checkErrorCode, checkResponseTime, checkStatusCode, checkUserData, checkUserDataAfterRegistration } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();
const userRegistration = new RegistrationController();
const chai = require("chai");
chai.use(require("chai-json-schema"));
const email = guid() + global.appConfig.users.firstUser.email;
const avatar = global.appConfig.users.firstUser.avatar;
const userName = global.appConfig.users.firstUser.userName;
const password = global.appConfig.users.firstUser.password;

xdescribe(`Check,if deleted user can create a post`, () => {
    let loginToken: string;
    let userId: number;

    it("Should register a new user", async () => {
        const response = await userRegistration.userRegistration(avatar, email, userName, password);
        checkStatusCode(response, 201);
        userId = response.body.user.id;
        checkUserDataAfterRegistration(response, avatar, userName, email, userId);
        loginToken = response.body.token.accessToken.token;
    });

    it("Delete current user", async () => {
        const response = await users.deleteUser(userId, loginToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });
    it("Try to login by deleted user", async () => {
        let loginResponse = await auth.login(email, password);

        checkStatusCode(loginResponse, 404);
        checkErrorCode(loginResponse, "Entity User was not found.", 2)
          
    });
});
