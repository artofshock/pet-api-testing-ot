import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { RegistrationController } from "../lib/controllers/registration.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { TokenController } from "../lib/controllers/token.controller";
import {  checkJsonSchema, checkResponseLenghtsMoreThanOne, checkResponseTime, checkStatusCode, checkUserData } from "../../helpers/functionsForChecking.helper";

const schemas = require("./data/schemas_registrUser.json");

const email = global.appConfig.users.firstUser.email;
const avatar = global.appConfig.users.firstUser.avatar;
const userName = global.appConfig.users.firstUser.userName;
const password = global.appConfig.users.firstUser.password;
const updatedEmail = "Updated@Email.com";
const updatedavatar = "updatedavatar";
const updatedUsername = "updatedusername";
const auth = new AuthController();
const users = new UsersController();
const userRegistration = new RegistrationController();

xdescribe("New user registration", () => {
    let loginToken: string;
    let userId: number;

    it("Should register a new user", async () => {
        const response = await userRegistration.userRegistration(avatar, email, userName, password);
        userId = response.body.user.id;
        checkStatusCode(response, 201);
        checkJsonSchema (response, schemas.schema_registrUser);
        expect(response.body.user.email).to.be.equal(email);
        
    });

    it("Get all user list", async () => {
        const response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        checkResponseLenghtsMoreThanOne(response);
    });

    it("Log in with valid credentials", async () => {
        const response = await auth.login(email, password);
        checkStatusCode(response, 200);

        loginToken = response.body.token.accessToken.token;
        userId = response.body.user.id;
    });

    it("Get details of the current logged in user by token", async () => {
        const response = await users.getUserbyToken(loginToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);

        checkUserData(response, avatar, userName, email, userId);
    });

    it("Update current user", async () => {
        let updatedUserData: object = {
            avatar: updatedavatar,
            email: updatedEmail,
            userName: updatedUsername,
            id: userId,
        };
        const response = await users.updateUser(updatedUserData, loginToken);
        checkStatusCode(response, 204);
        checkResponseTime(response, 1000)
    });

    it("Get details of the current logged-in user", async () => {
        const response = await users.getUserbyToken(loginToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkUserData(response, updatedavatar, updatedUsername, updatedEmail, userId);
    });

    it("Get details of current user by its id:", async () => {
        const response = await users.getUserbyId(userId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000)
        checkUserData(response, updatedavatar, updatedUsername, updatedEmail, userId);
    });
    it("Delete current user", async () => {
        const response = await users.deleteUser(userId, loginToken);
        checkStatusCode(response, 204);
       checkResponseTime(response, 1000);
    });
});
