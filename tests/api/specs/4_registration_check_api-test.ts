import {
    checkResponseTime,
    checkStatusCode, 
    checkJsonSchema,
    checkValidationErrorMessage,
} from '../../helpers/functionsForChecking.helper';




import { expect } from "chai";





import { RegistrationController } from "../lib/controllers/registration.controller";
import { AuthController } from '../lib/controllers/auth.controller';
import { UsersController } from '../lib/controllers/users.controller';

const userRegistration = new RegistrationController ();
const auth = new AuthController ();
const users =new UsersController ();
const email = global.appConfig.users.firstUser.email;
const password = global.appConfig.users.firstUser.password;


xdescribe('Use test data set for login', () => {
    let invalidRegistrationDataSet = [

        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: '' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: '123   1' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: '12345678901234567' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt @gmail.com', password: 'firstgmailcom' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailtgmail.com', password: 'firstgmai lcom' },
        { avatar: 'testavatar', userName: 'TestUserName', email: '@gmail.com', password: 'password' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@.com', password: 'password' },
        { avatar: 'testavatar', userName: 'TestUserName', email: '2@gmail.com', password: 'password' },
        { avatar: 'testavatar', userName: 'TestUserName', email: '1', password: 'passwor-d' },
        { avatar: 'testavatar', userName: 'Te', email: 'firstemailt@gmail.com', password: 'firstgmailcom'},
        { avatar: 'testavatar', userName: '    ', email: 'firstemailt@gmail.com', password: 'firstgmailcom'}
    ];
    let validRegistrationDataSet =  [
       
        { avatar: '', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: 'TESTS2023!' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: '123456789012345' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: '1234567890123456' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: '1234' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: '12345' },
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstemailt@gmail.com', password: ')&*&^#$*%^!sdfdf'},
        { avatar: 'testavatar', userName: 'TestUserName', email: 'firstem%%%?*@gmail.com', password: 'password' },
        { avatar: 'testavatar', userName: 'Tes', email: 'firstemailt@gmail.com', password: 'firstgmailcom' },
        { avatar: 'testavatar', userName: 'Test', email: 'firstemailt@gmail.com', password: 'firstgmailcom' },
    ];
    let loginToken: string;
    let userId: number;
    let userIdList = new Array();

    invalidRegistrationDataSet.forEach((credentials) => {
        it(`should not registration using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await userRegistration.userRegistration(credentials.avatar, credentials.email, credentials.userName,  credentials.password);
            checkStatusCode(response, 400); 
            checkResponseTime(response, 3000);
            checkValidationErrorMessage(response);
         
        });
    });
    validRegistrationDataSet.forEach((credentials) => {
        it(`should registration using valid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await userRegistration.userRegistration(credentials.avatar, credentials.email, credentials.userName,  credentials.password);
            checkStatusCode(response, 201); 
            checkResponseTime(response, 3000);
            userId = response.body.user.id;
            userIdList.push(userId);
        });
    });
    after('Let delete all registrated users', async () =>{
        let response = await auth.login(email, password);
        loginToken = response.body.token.accessToken.token;

        userIdList.forEach((id) => {
            it(`should delete user with valid credentials`, async () => {
                const deleteResponse = await users.deleteUser(id, loginToken);
                checkStatusCode(response, 204);
                checkResponseTime(response, 1000);
            });
             
            
        });
    })
});


