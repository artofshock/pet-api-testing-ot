import { expect } from "chai";
import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import { checkJsonSchema, checkResponseLenghtsMoreThanOne, checkResponseTime, checkStatusCode } from "../../helpers/functionsForChecking.helper";


const posts = new PostsController();
const comments = new CommentsController();
const users = new UsersController();
const auth = new AuthController();
const email = global.appConfig.users.firstUser.email;
const password = global.appConfig.users.firstUser.password;
const schemas = require("./data/schemas_allUsers.json");
const registrSchema = require("./data/schemas_registrUser.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));
const previewImage = "testimage";
const bodyValue = "BodyValueNewPost";
const bodyValueComment = "BodyValueComment";

describe(`Test post functionality`, () => {
    let loginToken: string;
    let userId: number;
    let postId: number;
    let commentId: number;

    before(`Login and get the token and userId`, async () => {
        let loginResponse = await auth.login(email, password);
        userId = loginResponse.body.user.id;
        checkJsonSchema(loginResponse,registrSchema)
        loginToken = loginResponse.body.token.accessToken.token;
    });

    it("Create new post for all users", async () => {
        let response = await posts.createNewPost(userId, previewImage, bodyValue, loginToken);
        postId = response.body.id;
        checkStatusCode(response, 200);
    });

    it("Add like reaction to post", async () => {
        let response = await posts.likeReaction(postId, userId, loginToken, true);
       checkStatusCode(response, 200);
    });

    it("Add new comment to post", async () => {
        let response = await comments.comments(userId, postId, bodyValueComment, loginToken);
        checkStatusCode(response, 200);
        expect(response.body.body, "Check comment is correct").to.be.equal(bodyValueComment);
        commentId = response.body.id

    });

    it(`should return all posts`, async () => {
        let response = await posts.getAllPosts();
        let post = response.body
            .map(function (x) {
                return x.id;
            })
            .indexOf(postId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1500);
        checkResponseLenghtsMoreThanOne(response);
        expect(post.reactions[0].isLike).to.be.equal(true);
        expect(post.comments[0].id).to.be.equal(commentId);
        expect(post.comments[0].body).to.be.equal(commentId);
    });
});
