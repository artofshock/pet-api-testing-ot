import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl

export class CommentsController {
    async comments(authorId: number, postId: number, bodyValue: string, accessToken:string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/comments`)
            .body({
                authorId: authorId,
                postId: postId,
                body: bodyValue
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
