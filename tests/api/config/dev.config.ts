global.appConfig = {
    envName: "DEV Environment",
    baseUrl: "http://tasque.lol/",
    swaggerUrl: "http://tasque.lol/index.html",

    users: {
        firstUser: {
            email: "dev@example.com",
            password: "testPassword1",
            avatar: "avatardev",
            userName: "usernamedev",
        },
    },
};
